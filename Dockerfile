FROM registry.gitlab.com/gbraad/fedora:24
MAINTAINER Gerard Braad <me@gbraad.nl>

RUN dnf install -y tar wget findutils; \
    mkdir -p /workspace

VOLUME /workspace
WORKDIR /workspace

RUN wget --quiet --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u101-b13/jdk-8u101-linux-x64.rpm" -O jdk.rpm ; \
    rpm -ivh jdk.rpm ; \
    rm -f jdk.rpm
    
RUN cd /opt ; \
    curl --silent https://dl.google.com/android/android-sdk_r24.4.1-linux.tgz -o android-sdk.tgz ; \
    tar -zxvf android-sdk.tgz ; \
    rm -f android-sdk.tgz ; \
    mv android-sdk-linux android-sdk

ENV ANDROID_HOME /opt/android-sdk
ENV PATH $PATH:$ANDROID_HOME/tools
ENV PATH $PATH:$ANDROID_HOME/platform-tools

RUN echo "y" | android update sdk --no-ui --force --filter 1,2,3,4,5,6,7,8,9,10
